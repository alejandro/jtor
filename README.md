# JtoR
A Java to (J)Ruby translator.

## Installation
`jruby -S gem install jtor-0.1.0.gem`

## Usage
`jruby -S jtor source_dir destination_dir`

## For contributors

You'll need [JRuby](http://jruby.org/)

### Setup
````
git clone git@gitlab.com:eReGeBe/jtor.git
cd jtor
jruby -S bundle install
````

## Licence
Copyright (c) 2016 Alejandro Rodríguez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## TODO
- Handle labeled `break`s and `continue`s
- Handle object creations with anonymous classes
- Handle class expressions on `this`
- Handle var args (...)
