module Jtor
  module Expressions
    include_package 'com.github.javaparser.ast.expr'
    include_package 'com.github.javaparser.ast.type'

    ASSINGMENT_OPERATORS = {
      assign: '=',
      plus: '+=',
      minus: '-=',
      star: '*=',
      slash: '/=',
      and: '&=',
      or: '|=',
      xor: '^=',
      rem: '%=',
      lShift: '<<=',
      rSignedShift: '>>=',
      rUnsignedShift: '>>>='
    }

    UNARY_OPERATORS = {
      positive: '+',
      negative: '-',
      preIncrement: '++',
      preDecrement: '--',
      not: '!',
      inverse: '~',
      posIncrement: '++',
      posDecrement: '--'
    }

    BINARY_OPERATORS = {
      or: '||',
      and: '&&',
      binOr: '|',
      binAnd: '&',
      xor: '^',
      equals: '==',
      notEquals: '!=',
      less: '<',
      greater: '>',
      lessEquals: '<=',
      greaterEquals: '>=',
      lShift: '<<',
      rSignedShift: '>>',
      rUnsignedShift: '>>>',
      plus: '+',
      minus: '-',
      times: '*',
      divide: '/',
      remainder: '%'
    }

    def translate_expression(expr)
      case expr
      when MethodCallExpr
        scoped_expression(expr) do
          "#{expr.name}#{translate_arguments(expr.args)}"
        end
      when AssignExpr
        target = translate_expression(expr.target)
        operator = ASSINGMENT_OPERATORS[expr.operator.to_s.to_sym]
        value = translate_expression(expr.value)
        "#{target} #{operator} #{value}"
      when FieldAccessExpr
        scoped_expression(expr) { expr.field }
      when VariableDeclarationExpr
        expr.vars.map do |var|
          # If only declaring and not assigning, we don't need to translate it
          # (I think)
          next unless var.init
          "#{var.id} = #{translate_expression(var.init)}"
        end.compact.join('; ')
      when QualifiedNameExpr
        "#{translate_expression(expr.qualifier)}.#{expr.name}"
      when NameExpr
        expr.name
      when ArrayAccessExpr
        "#{translate_expression(expr.name)}[#{translate_expression(expr.index)}]"
      when ArrayCreationExpr
        if expr.initializer
          translate_expression(expr.initializer)
        else
          "Array.new(#{expr.array_count})"
        end
      when ArrayInitializerExpr
        values = expr.values.map { |value| translate_expression(value) }
        "[#{values.join(', ')}]"
      when UnaryExpr
        op = expr.operator.to_s.to_sym
        operator = UNARY_OPERATORS[op]
        value = translate_expression(expr.expr)
        case op
        when :posIncrement
          # For preIncrement and preDecrement we record the old value first,
          # then execute the operation and then return the old value
          "(__old = #{value}; #{value} += 1; __old)"
        when :posDecrement
          "(__old = #{value}; #{value} -= 1; __old)"
        when :preIncrement
          # No ++/-- in ruby
          "(#{value} += 1)"
        when :preDecrement
          "(#{value} -= 1)"
        else
          "#{operator}#{value}"
        end
      when BinaryExpr
        left = translate_expression(expr.left)
        operator = BINARY_OPERATORS[expr.operator.to_s.to_sym]
        right = translate_expression(expr.right)
        "#{left} #{operator} #{right}"
      when IntegerLiteralExpr
        # NOTE: `IntegerLiteralExpr` is a subclass of `StringLiteralExpr`, so
        # this case must be before the latter
        expr.value
      when StringLiteralExpr
        expr.value.inspect
      when BooleanLiteralExpr
        expr.value
      when ObjectCreationExpr
        # TODO: Handle object creations with anonymous classes
        scoped_expression(expr) do
          type = translate_type(expr.type)
          "#{type}.new#{translate_arguments(expr.args)}"
        end
      when InstanceOfExpr
        # No `instanceof` operator in ruby, but every expression is an object
        # with the `is_a?` method, which serves the same purpose. We wrap the
        # expression in parenthesis just in case it's not atomic.
        "(#{translate_expression(expr.expr)}).is_a?(#{translate_type(expr.type)})"
      when NullLiteralExpr
        'nil'
      when CastExpr
        # No need to cast anything :P
        translate_expression(expr.expr)
      when ThisExpr
        # TODO: Handle class expressions on `this`
        'self'
      when SuperExpr
        # Return our trusty `sup` helper object which encapsulates super methods
        # TODO: Handle class expressions on `super`
        'sup'
      when ClassExpr
        "#{translate_type(expr.type)}.class"
      when EnclosedExpr
        "(#{translate_expression(expr.inner)})"
      when ConditionalExpr
        condition = translate_expression(expr.condition)
        then_expr = translate_expression(expr.then_expr)
        else_expr = translate_expression(expr.else_expr)
        "#{condition} ? #{then_expr} : #{else_expr}"
      end
    end

    def translate_expressions(expressions)
      expressions.map { |expr| translate_expression(expr) }.join('; ')
    end

    def translate_arguments(args)
      return '' unless args && args.any?
      "(#{args.map { |arg| translate_expression(arg) }.join(', ')})"
    end

    def translate_constructor_call_args(params, args)
      # These arguments will be `eval`ed in Jtor::StdLib::Base. There the
      # arguments are in an array called `args`. We'll translate any reference
      # to a name defined in `params` to `args[i]`, where `i` is the index of
      # that parameter in `params`
      arguments = translate_arguments(args)
      params.each_with_index do |param, index|
        replacement = "\\1args[#{index}]\\2"
        # Only replace unscoped names matching the param name
        arguments.gsub!(/([ \(])#{param.id.name}([., \)])/, replacement)
      end
      arguments
    end

    private

    def translate_type(type)
      case type
      when ClassOrInterfaceType
        if type.scope
          scope = type
          while (scope = scope.scope)
            module_string = "#{scope.name.to_constant_name}#{module_string}"
          end
          module_string += '::'
        elsif %w(Exception String Math Class Object).include?(type.name)
          module_string = 'JavaLang::'
        end
        "#{module_string}#{type.name}"
      when PrimitiveType
        # Mapping from
        # https://github.com/jruby/jruby/wiki/CallingJavaFromJRuby#java-to-ruby
        case type.type
        when PrimitiveType::Primitive::Boolean
          'JavaLang::Boolean'
        when PrimitiveType::Primitive::Byte, PrimitiveType::Primitive::Short,
              PrimitiveType::Primitive::Char, PrimitiveType::Primitive::Int,
              PrimitiveType::Primitive::Long
          'Fixnum'
        when PrimitiveType::Primitive::Float, PrimitiveType::Primitive::Double
          'Float'
        end
      when ReferenceType
        # Integer[], String[], Float[][] all translates to Array in ruby
        type.array_count == 0 ? translate_type(type.type) : 'Array'
      end
    end

    def scoped_expression(expr)
      scope = "#{translate_expression(expr.scope)}." if expr.scope
      "#{scope}#{yield}"
    end
  end
end
