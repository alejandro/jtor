require 'fileutils'
require 'erb'

module Jtor
  class Translator
    def self.run(src, dest)
      gemfile_template = ERB.new(File.read('templates/Gemfile.erb'))
      Dir.chdir(src)
      # Write Gemfile
      File.open(File.join(dest, 'Gemfile'), 'w') do |f|
        f.puts(gemfile_template.result(binding))
      end

      # Translate files
      Dir.glob('**/*.java').each do |file|
        dest_file = File.join(dest, file.gsub(/java$/, 'rb'))
        FileUtils.mkdir_p(File.dirname(dest_file))
        File.open(dest_file, 'w') do |f|
          f.puts("require 'jtor-stdlib'")
          JavaParser.new(f).translate(File.join(src, file))
        end
      end
    end
  end
end
