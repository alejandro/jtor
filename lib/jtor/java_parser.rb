require_relative 'expressions'

java_import java.io.FileInputStream
java_import com.github.javaparser.JavaParser

module Jtor
  class JavaParser
    include_package 'com.github.javaparser.ast.stmt'
    include_package 'com.github.javaparser.ast.body'

    include Jtor::Expressions

    attr_accessor :indentator

    def initialize(f)
      @indentator = '  '
      @level = 0
      @f = f
    end

    def translate(file)
      parser = ::JavaParser.parse(FileInputStream.new(file))
      # In Java, other classes on the same package are automatically accessible,
      # we do that manually here.
      fputs("jtor_import '#{parser.package.name}.*'")
      fputs('module Java')
      indented do
        fputs("module #{module_name(parser.package.name)}")
        indented do
          parser.imports.each do |import|
            fputs("jtor_import '#{translate_expression(import.name)}'")
          end
          parser.types.each { |type| translate_class_or_interface(type) }
        end
        fputs('end')
      end
      fputs('end')
    end

    private

    def indent
      @level += 1
    end

    def outdent
      @level -= 1
    end

    def indented
      indent
      yield
      outdent
    end

    def indented_string(s)
      # If we last used a `write` or if the string passed is empty, we don't
      # want indentation
      if @prev_write || !s
        s
      else
        "#{@indentator * @level}#{s}"
      end
    end

    def fwrite(s)
      @f.write(indented_string(s))
      @prev_write = true
    end

    def fputs(s = nil)
      @f.puts(indented_string(s))
      @prev_write = false
    end

    def translate_statement(stmt)
      case stmt
      when BlockStmt
        translate_block_statement(stmt)
      when ExpressionStmt
        translated_expression = translate_expression(stmt.expression)
        fputs(translated_expression) if translated_expression
      when IfStmt
        fputs("if #{translate_expression(stmt.condition)}")
        indented { translate_statement(stmt.then_stmt) }
        if stmt.else_stmt
          if stmt.else_stmt.is_a?(IfStmt)
            fwrite('els')
            translate_statement(stmt.else_stmt)
          else
            fputs('else')
            indented { translate_statement(stmt.then_stmt) }
            fputs('end')
          end
        else
          fputs('end')
        end
      when TryStmt
        fputs('begin')
        indented { translate_block_statement(stmt.try_block) }
        stmt.catchs.each do |catch_clause|
          except = catch_clause.except
          fputs("rescue #{except.types[0].type.name} => #{except.id.name}")
          indented { translate_block_statement(catch_clause.catch_block) }
        end
        if stmt.finally_block
          fputs('ensure')
          indented { translate_block_statement(stmt.finally_block) }
        end
        fputs('end')
      when ForStmt
        fputs(translate_expressions(stmt.init)) if stmt.init.any?
        fputs("while #{translate_expression(stmt.compare)} do")
        indented do
          translate_statement(stmt.body)
          fputs(translate_expressions(stmt.update)) if stmt.update.any?
        end
        fputs('end')
      when ForeachStmt
        iterable = translate_expression(stmt.iterable)
        var = stmt.variable.vars.first.id.to_s
        fputs("#{iterable}.each do |#{var}|")
        indented { translate_statement(stmt.body) }
        fputs('end')
      when ReturnStmt
        "return #{translate_expression(stmt.expr)}"
      when ThrowStmt
        "fail #{translate_expression(stmt.expr)}"
      when BreakStmt
        # TODO: Handle labeled `break`s
        'break'
      when ContinueStmt
        # TODO: Handle labeled `continue`s
        'next'
      when DoStmt
        fputs('begin')
        indented { translate_statement(stmt.body) }
        fputs("end while #{translate_expression(stmt.condition)}")
      when SwitchStmt
        fputs("case #{translate_expression(stmt.selector)}")
        stmt.entries.each do |entry|
          fputs(entry.label ? "when #{entry.label}" : 'else')
          indented do
            entry.stmts.each { |stmt| translate_statement(stmt) }
          end
        end
        fputs('end')
      end
    end

    def translate_block_statement(block_stmt)
      block_stmt.stmts.each { |stmt| translate_statement(stmt) }
    end

    def translate_param_types(params)
      "[#{params.map { |p| translate_type(p.type) }.join(', ')}]"
    end

    def translate_param_list(params)
      params.any? ? " |#{params.map { |p| p.id.name }.join(', ')}|" : ''
    end

    def write_method(method)
      types = translate_param_types(method.parameters)
      param_list = translate_param_list(method.parameters)
      declarer_method = if ModifierSet.static?(method.modifiers)
        'add_static_java_method'
      else
        'add_java_method'
      end
      fputs("#{declarer_method}(:#{method.name}, #{types}) do#{param_list}")
      indented { translate_block_statement(method.body) } if method.body
      fputs('end')
    end

    def write_constructor(constructor)
      types = translate_param_types(constructor.parameters)
      param_list = translate_param_list(constructor.parameters)
      stmts = constructor.block && constructor.block.stmts
      if stmts && stmts.any? && stmts.first.is_a?(ExplicitConstructorInvocationStmt)
        constructor_stmt = stmts.first
        stmts = stmts.size > 1 ? stmts.sub_list(1, stmts.size - 1) : []
        super_call = constructor_stmt.this? ? 'initialize' : 'super'
        super_call += translate_constructor_call_args(constructor.parameters,
            constructor_stmt.args)
      end
      # Using `#` as the delimiter, as it can't appear on Java expressions
      fputs("add_java_constructor(#{types}, %q##{super_call}#) do#{param_list}")
      indented do
        stmts.each { |stmt| translate_statement(stmt) }
      end if stmts
      fputs('end')
    end

    def write_field(field)
      field.variables.each do |var|
        fputs "attr_accessor :#{var.id}"
        next unless var.init
        fputs "add_java_initializer do"
        indented { fputs "self.#{var.id} ||= #{translate_expression(var.init)}" }
        fputs "end"
      end
    end

    def write_static_field(field)
      field.variables.each do |var|
        fputs "class << self; attr_accessor :#{var.id}; end"
        fputs "@#{var.id} ||= #{translate_expression(var.init)}" if var.init
      end
    end

    def translate_member(member)
      case member
      when MethodDeclaration
        write_method(member)
      when ConstructorDeclaration
        write_constructor(member)
      when FieldDeclaration
        if ModifierSet.static?(member.modifiers)
          write_static_field(member)
        else
          write_field(member)
        end
      end
    end

    def translate_class_or_interface(type)
      if type.extends.any?
        extends = " < #{type.extends.map { |e| translate_type(e) }.join(', ')}"
      end
      fputs("class #{type.name.to_constant_name}#{extends}")
      indented do
        fputs('include Jtor::StdLib::Base')
        type.members.each { |member| translate_member(member) }
      end
      fputs('end')
    end

    def module_name(name_expr)
      name_expr.name.split('.').map(&:to_constant_name).join
    end
  end
end
