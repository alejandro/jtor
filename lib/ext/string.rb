class String
  def to_constant_name
    # ruby class names must start with an uppercase letter (Java types may not)
    empty? ? self : "#{self[0].upcase}#{self[1..-1]}"
  end
end
