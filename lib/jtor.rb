warn 'Loading jtor in a non-JRuby interpreter' unless defined? JRUBY_VERSION

require 'java'

require 'ext/string'

require 'jtor_jars.rb'

require 'jtor/version'
require 'jtor/java_parser'
require 'jtor/translator'
