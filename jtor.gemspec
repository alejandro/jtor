$:.push File.expand_path("../lib", __FILE__)
require 'jtor/version'

Gem::Specification.new do |s|
  s.platform     = 'java'
  s.name         = 'jtor'
  s.version      = Jtor::VERSION
  s.date         = '2016-02-28'
  s.summary      = 'JtoR - Java to Ruby translator'
  s.description  = 'A Java to (J)Ruby translator'
  s.authors      = ['Alejandro Rodríguez']
  s.email        = 'alejandroluis24@gmail.com'
  s.homepage     = 'https://gitlab.com/eReGeBe/jtor'
  s.files        = Dir['{lib}/**/*.rb', 'bin/*']
  s.executables  << 'jtor'
  s.requirements << 'jar com.github.javaparser:javaparser-core, 2.3.0'
  s.add_runtime_dependency 'jar-dependencies', '~> 0.3.2'
  s.license      = 'MIT'
end
